package service

import (
	"context"
	"errors"
	"fmt"
	"github.com/teris-io/shortid"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"stoik-url-shortener/internal/config"
	"time"
)

var (
	ErrSlugNotFound = errors.New("slug not found")
	ErrSlugExpired  = errors.New("url has expired")
)

type Shortener interface {
	Shorten(context.Context, string) (string, error)
	GetShortened(context.Context, string) (string, error)
}

type shortener struct {
	db *gorm.DB
}

func NewShortener() Shortener {
	dsn := config.GetDatabaseConfig().PgCxURL()
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(fmt.Errorf("could not connect to database: %s", err))
	}
	if err := db.AutoMigrate(&URL{}); err != nil {
		panic(err)
	}
	return &shortener{db: db}
}

func (s *shortener) Shorten(ctx context.Context, url string) (string, error) {
	// First we fetch for an existing not expired shortened URL for this URL.
	var existingURL URL
	result := s.db.WithContext(ctx).Where("original_url = ? AND expires_at > ?", url, time.Now()).First(&existingURL)
	if result.Error == nil {
		return existingURL.GetShortenedURL(), nil
	} else if !errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return "", fmt.Errorf("could not check existing URL: %w", result.Error)
	}

	u := URL{
		OriginalURL: url,
		Slug:        shortid.MustGenerate(),
		Domain:      config.GetApplicationConfig().Domain,
		CreatedAt:   time.Now(),
		Clicks:      0,
		ExpiresAt:   time.Now().Add(time.Second * time.Duration(config.GetApplicationConfig().LinkSecondsDuration)),
	}

	if err := s.db.WithContext(ctx).Create(&u).Error; err != nil {
		return "", fmt.Errorf("could not create entry in database: %w", err)
	}

	log.Printf("url %s shortened to %s", u.OriginalURL, u.GetShortenedURL())

	return u.GetShortenedURL(), nil
}

func (s *shortener) GetShortened(ctx context.Context, slug string) (string, error) {
	var url URL
	if err := s.db.WithContext(ctx).Where("slug = ?", slug).First(&url).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return "", ErrSlugNotFound
		}
		return "", err
	}

	// Check expiration
	if url.ExpiresAt.Before(time.Now()) {
		log.Printf("shortened url %s for url %s has expired", url.GetShortenedURL(), url.OriginalURL)
		return "", ErrSlugExpired
	}

	// Increment click count
	s.db.WithContext(ctx).Model(&url).Update("clicks", gorm.Expr("clicks + 1"))

	log.Printf("shortened url %s from url %s clicked for the %dth time",
		url.GetShortenedURL(), url.OriginalURL, url.Clicks+1)

	return url.OriginalURL, nil
}
