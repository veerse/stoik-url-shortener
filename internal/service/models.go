package service

import (
	"time"
)

type URL struct {
	ID          uint      `gorm:"primary_key"`
	OriginalURL string    `gorm:"not null"`
	Domain      string    `gorm:"not null"`
	Slug        string    `gorm:"unique;not null"`
	CreatedAt   time.Time `gorm:"default:current_timestamp"`
	ExpiresAt   time.Time
	Clicks      int `gorm:"default:0"`
}

func (u *URL) GetShortenedURL() string {
	return u.Domain + "/" + u.Slug
}
