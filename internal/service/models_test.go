package service

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestURL_GetShortenedURL(t *testing.T) {
	u := URL{
		Domain: "http://tiny.io",
		Slug:   "slug",
	}
	require.Equal(t, "http://tiny.io/slug", u.GetShortenedURL())
}
