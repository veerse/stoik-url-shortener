package config

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/kelseyhightower/envconfig"
	"net"
	"strconv"
	"sync"
)

type APIConfig struct {
	Host string `split_words:"true" validate:"required"`
	Port int    `split_words:"true" validate:"required"`
}

func (c *APIConfig) ListenAddr() string {
	return net.JoinHostPort(c.Host, strconv.Itoa(c.Port))
}

type DatabaseConfig struct {
	Host     string `split_words:"true" validate:"required"`
	Port     int    `split_words:"true" validate:"required"`
	User     string `split_words:"true" validate:"required"`
	Password string `split_words:"true" validate:"required"`
	DbName   string `split_words:"true" validate:"required"`
}

func (c *DatabaseConfig) PgCxURL() string {
	return fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s sslmode=disable",
		c.Host, c.Port, c.User, c.DbName, c.Password)
}

type ApplicationConfig struct {
	Domain              string `split_words:"true" validate:"required"`
	LinkSecondsDuration int    `split_words:"true" validate:"required"`
}

var (
	dbConfigOnce, apiConfigOnce, appConfigOnce sync.Once
	dbConfig                                   DatabaseConfig
	apiConfig                                  APIConfig
	applicationConfig                          ApplicationConfig
)

func GetDatabaseConfig() *DatabaseConfig {
	dbConfigOnce.Do(func() {
		if err := envconfig.Process("DB", &dbConfig); err != nil {
			panic(err)
		}
		mustValidate(dbConfig)
	})
	return &dbConfig
}

func GetAPIConfig() *APIConfig {
	apiConfigOnce.Do(func() {
		if err := envconfig.Process("API", &apiConfig); err != nil {
			panic(err)
		}
		mustValidate(apiConfig)
	})
	return &apiConfig
}

func GetApplicationConfig() *ApplicationConfig {
	appConfigOnce.Do(func() {
		if err := envconfig.Process("APP", &applicationConfig); err != nil {
			panic(err)
		}
		mustValidate(applicationConfig)
	})
	return &applicationConfig
}

func mustValidate(s any) {
	if err := validator.New(validator.WithRequiredStructEnabled(),
		validator.WithPrivateFieldValidation()).Struct(s); err != nil {
		panic(err)
	}
}
