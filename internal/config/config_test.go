package config

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetDatabaseConfig(t *testing.T) {
	t.Setenv("DB_HOST", "foo")
	t.Setenv("DB_PORT", "5432")
	t.Setenv("DB_USER", "User")
	t.Setenv("DB_PASSWORD", "pwd")
	t.Setenv("DB_DB_NAME", "dbname")

	expected := "Host=foo Port=5432 User=User dbname=dbname Password=pwd sslmode=disable"
	dbConfig := GetDatabaseConfig()
	assert.Equal(t, expected, dbConfig.PgCxURL())
}

func TestGetAPIConfig(t *testing.T) {
	t.Setenv("API_HOST", "localhost")
	t.Setenv("API_PORT", "8081")

	expectedAddr := "localhost:8081"
	apiConfig := GetAPIConfig()
	assert.Equal(t, expectedAddr, apiConfig.ListenAddr())
}

func TestGetApplicationConfig(t *testing.T) {
	t.Setenv("APP_DOMAIN", "http://test")
	t.Setenv("APP_LINK_SECONDS_DURATION", "60")

	appConfig := GetApplicationConfig()
	assert.Equal(t, "http://test", appConfig.Domain)
	assert.Equal(t, 60, appConfig.LinkSecondsDuration)
}
