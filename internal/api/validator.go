package api

import (
	"errors"
	"fmt"
	"github.com/go-playground/validator/v10"
	"strings"
)

type BodyValidator struct {
	v *validator.Validate
}

func NewBodyValidator() *BodyValidator {
	return &BodyValidator{v: validator.New()}
}

// Validate ensures that the required fields of a body are respected.
func (qv *BodyValidator) Validate(i interface{}) error {
	if err := qv.v.Struct(i); err != nil {
		var validationErrors validator.ValidationErrors
		if errors.As(err, &validationErrors) {
			errorMessages := make([]string, len(validationErrors))
			for i, validationErr := range validationErrors {
				errorMessages[i] = fmt.Sprintf("field '%s' failed validation rule '%s'",
					validationErr.Field(),
					validationErr.Tag())
			}
			return fmt.Errorf("query validation failed: %s", strings.Join(errorMessages, ", "))
		}
		return fmt.Errorf("an internal error occurred: %v", err)
	}
	return nil
}
