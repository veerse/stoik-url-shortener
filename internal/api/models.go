package api

type ShortenRequest struct {
	URL string `json:"url" validate:"required,url"`
}
