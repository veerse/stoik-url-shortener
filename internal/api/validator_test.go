package api

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBodyValidator_Validate(t *testing.T) {
	type testStruct struct {
		Name string `validate:"required,min=2"`
		Age  int    `validate:"required,min=18"`
	}

	validator := NewBodyValidator()

	t.Run("valid-struct", func(t *testing.T) {
		validStruct := testStruct{
			Name: "John Doe",
			Age:  30,
		}
		assert.NoError(t, validator.Validate(validStruct))
	})

	t.Run("invalid-struct", func(t *testing.T) {
		invalidStruct := testStruct{
			Name: "J",
			Age:  17,
		}
		assert.Error(t, validator.Validate(invalidStruct))
	})
}
