package api

import (
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"stoik-url-shortener/internal/service"
	"strings"
	"testing"
)

func TestHandlers_HandleShorten(t *testing.T) {
	e := echo.New()
	e.Validator = NewBodyValidator()

	t.Run("nominal-case", func(t *testing.T) {
		mockService := new(service.MockShortener)
		handlers := NewHandlers(mockService)
		req := httptest.NewRequest(http.MethodPost,
			"/shorten",
			strings.NewReader(`{"url":"http://www.foo.fr"}`))

		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		httpRecorder := httptest.NewRecorder()
		ctx := e.NewContext(req, httpRecorder)

		mockService.On("Shorten", mock.Anything, "http://www.foo.fr").
			Return("http://www.bar.fr", nil)

		assert.Equal(t, http.StatusOK, httpRecorder.Code)
		assert.NoError(t, handlers.HandleShorten(ctx))
		mockService.AssertExpectations(t)
	})

	t.Run("incorrect-body", func(t *testing.T) {
		mockService := new(service.MockShortener)
		handlers := NewHandlers(mockService)
		req := httptest.NewRequest(http.MethodPost,
			"/shorten",
			strings.NewReader(`{"url":"1234"}`))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		httpRecorder := httptest.NewRecorder()
		ctx := e.NewContext(req, httpRecorder)
		assert.Error(t, handlers.HandleShorten(ctx))
	})
}

func TestHandlers_RedirectToShortened(t *testing.T) {
	e := echo.New()

	t.Run("nominal-case", func(t *testing.T) {
		mockService := new(service.MockShortener)
		handlers := NewHandlers(mockService)
		req := httptest.NewRequest(http.MethodGet, "/{slug}", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		httpRecorder := httptest.NewRecorder()
		ctx := e.NewContext(req, httpRecorder)
		ctx.SetParamNames("slug")
		ctx.SetParamValues("validSlug")

		mockService.On("GetShortened", mock.Anything, "validSlug").
			Return("http://original.com", nil)

		assert.NoError(t, handlers.RedirectToShortened(ctx))
		assert.Equal(t, http.StatusMovedPermanently, httpRecorder.Code)
		assert.Equal(t, "http://original.com", httpRecorder.Header().Get("Location"))
		mockService.AssertExpectations(t)
	})

	t.Run("slug-missing", func(t *testing.T) {
		handlers := NewHandlers(nil) // No need for a mock service as the handler should fail before reaching the service layer.
		req := httptest.NewRequest(http.MethodGet, "/{slug}", nil)
		httpRecorder := httptest.NewRecorder()
		ctx := e.NewContext(req, httpRecorder)
		assert.Error(t, handlers.RedirectToShortened(ctx))
	})

	t.Run("slug-not-found", func(t *testing.T) {
		mockService := new(service.MockShortener)
		handlers := NewHandlers(mockService)
		req := httptest.NewRequest(http.MethodGet, "/{slug}", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		httpRecorder := httptest.NewRecorder()
		ctx := e.NewContext(req, httpRecorder)
		ctx.SetParamNames("slug")
		ctx.SetParamValues("invalidSlug")

		mockService.On("GetShortened", mock.Anything, "invalidSlug").Return("", echo.ErrNotFound)
		assert.Error(t, handlers.RedirectToShortened(ctx))
	})
}
