package api

import "github.com/labstack/echo/v4"

type API struct {
	e *echo.Echo
}

func NewAPI(h Handlers) *echo.Echo {
	e := echo.New()
	e.Validator = NewBodyValidator()
	e.HidePort = true
	e.HideBanner = true
	e.POST("/shorten", h.HandleShorten)
	e.GET("/:slug", h.RedirectToShortened)
	return e
}
