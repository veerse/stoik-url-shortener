package api

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"stoik-url-shortener/internal/service"
)

type Handlers interface {
	HandleShorten(c echo.Context) error
	RedirectToShortened(c echo.Context) error
}

type handlers struct {
	shortener service.Shortener
}

func NewHandlers(shortener service.Shortener) Handlers {
	return &handlers{
		shortener: shortener,
	}
}

func (h *handlers) HandleShorten(c echo.Context) error {
	var input ShortenRequest
	if err := bindAndValidate(c, &input); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	shortened, err := h.shortener.Shorten(c.Request().Context(), input.URL)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, shortened)
}

func (h *handlers) RedirectToShortened(c echo.Context) error {
	slug := c.Param("slug")
	if slug == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "slug required")
	}

	original, err := h.shortener.GetShortened(c.Request().Context(), slug)
	if err != nil {
		return echo.NewHTTPError(http.StatusNotFound, err)
	}

	return c.Redirect(http.StatusMovedPermanently, original)
}

func bindAndValidate(c echo.Context, input interface{}) error {
	if err := c.Bind(input); err != nil {
		return err
	}
	return c.Validate(input)
}
