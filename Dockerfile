FROM golang:1.22 as builder

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o shortener-api cmd/stoik-url-shortener/main.go

FROM alpine:3.9

RUN apk add --no-cache ca-certificates \
    && apk add --no-cache wget \
    && wget https://github.com/jwilder/dockerize/releases/download/v0.6.1/dockerize-alpine-linux-amd64-v0.6.1.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-v0.6.1.tar.gz \
    && rm dockerize-alpine-linux-amd64-v0.6.1.tar.gz

COPY --from=builder /app/shortener-api /shortener-api

EXPOSE 8081

CMD dockerize -wait tcp://db:5432 -timeout 60s /shortener-api