# URL Shortener Service

Tiny URL shortener POC made for STOIK.

## Features

- **Shorten URLs**: Convert long URLs into short.
- **Redirect**: Seamlessly redirect from a shortened URL to its original URL.
- **Persistence**: Store URL mappings in SQL database.
- **Expiration**: Expiration dates on URLs.
- **Count**: Tracking of click numbers.

## Run the application

### Prerequisites

- Docker 
- Docker-compose
- Make
- Empty port 8081 :-)

### How to run

    make start

### How to query

Shorten an URL:

    curl --location --request POST 'localhost:8081/shorten' \
    --header 'Content-Type: application/json' \
    --data-raw '{
    "url": "https://medium.com/equify-tech/the-three-fundamental-stages-of-an-enginering-career-54dac732fc74"
    }'

Be redirected to the original website after shortening :

    curl --location --request GET 'http://localhost:8081/xvAqdCBIR'

## To go to prod

- Set up a reliable and scalable infrastructure (Kubernetes cluster for instance).
- Set up CI/CD pipeline for automated testing and deployment.
- Implement security measures like HTTPS, authentication, and rate limiting.
- Plan for database backups.
- Implement analytics and reporting for URL clicks and usage metrics (Prometheus,...).