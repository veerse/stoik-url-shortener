package main

import (
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"stoik-url-shortener/internal/api"
	"stoik-url-shortener/internal/config"
	"stoik-url-shortener/internal/service"
)

func main() {
	log.Printf("starting api on %s", config.GetAPIConfig().ListenAddr())
	s := api.NewAPI(api.NewHandlers(service.NewShortener()))
	if err := s.Start(config.GetAPIConfig().ListenAddr()); err != nil {
		log.Fatal(err)
	}
}
